#pragma once

#include <stdint.h>
#include <stdlib.h>

/// Initialize the bus with the given maximum clock rate in Khz
void ssp2lab__init(uint32_t max_clock_khz);

uint8_t ssp2__data_exchange(uint8_t data_out);

// /**
//  * Exchange a single byte over the SPI bus
//  * @returns the byte received while sending the byte_to_transmit
//  */
// uint8_t ssp2lab__exchange_byte(uint8_t byte_to_transmit);

// /**
//  * @{
//  * @name    Exchanges larger blocks over the SPI bus
//  * These are designed to be one-way transmission for the SPI for now
//  */
// void ssp2lab__dma_write_block(const unsigned char *output_block, size_t number_of_bytes);
// void ssp2lab__dma_read_block(unsigned char *input_block, size_t number_of_bytes);
// /** @} */

#include <stdbool.h>
#include <stddef.h>

#include "ssp2lab.h"

#include "clock.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

void ssp2lab__init(uint32_t max_clock_khz) {
  // enabling power
  LPC_SC->PCONP |= (1 << 20);
  // 8 bit standard transfer
  LPC_SSP2->CR0 = (0b111 << 0);
  LPC_SSP2->CR1 = (0b1 << 1);
  LPC_SSP2->CPSR = 96;
}

uint8_t ssp2__data_exchange(uint8_t data_out) {
  LPC_SSP2->DR = data_out;

  while (LPC_SSP2->SR & (1 << 4)) {
    ;
  }

  return LPC_SSP2->DR;
}
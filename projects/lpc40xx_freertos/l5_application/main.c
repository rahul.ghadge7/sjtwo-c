#include <stdio.h>

#include "FreeRTOS.h"
#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "periodic_scheduler.h"
#include "queue.h"
#include "sj2_cli.h"
#include "ssp2lab.h"
#include "task.h"

#include <stdio.h>

#include "FreeRTOS.h"
#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "periodic_scheduler.h"
#include "queue.h"
#include "sj2_cli.h"
#include "ssp2lab.h"
#include "task.h"

gpio_s gpio_cs;

void adesto_cs(void) { gpio__reset(gpio_cs); }
void adesto_ds(void) { gpio__set(gpio_cs); }

typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

adesto_flash_id_s adesto_read_signature(void) {

  adesto_flash_id_s data;

  const uint8_t read_opcode = 0x9f;
  const uint8_t dummy_opcode = 0xff;
  data.manufacturer_id = ssp2__data_exchange(dummy_opcode);
  data.device_id_1 = ssp2__data_exchange(dummy_opcode);
  data.device_id_2 = ssp2__data_exchange(dummy_opcode);
  data.extended_device_id = ssp2__data_exchange(dummy_opcode);

  adesto_cs();
  {
    ssp2lab__init(read_opcode);
    // Send opcode and read bytes
    // TODO: Populate members of the 'adesto_flash_id_s' struct
  }
  adesto_ds();

  return data;
}

todo_configure_your_ssp2_pin_functions() {
  LPC_IOCON->P1_0 &= ~0b111;
  LPC_IOCON->P1_0 |= 0b100;

  LPC_IOCON->P1_1 &= ~0b111;
  LPC_IOCON->P1_1 |= 0b100;

  LPC_IOCON->P1_4 &= ~0b111;
  LPC_IOCON->P1_4 |= ~0b100;

  LPC_GPIO1->DIR &= ~(1 << 10);
  LPC_GPIO1->DIR |= (1 << 10);
  LPC_GPIO1->DIR |= (1 << 7);
}

void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 24;
  ssp2lab__init(spi_clock_mhz);

  todo_configure_your_ssp2_pin_functions();

  while (1) {
    adesto_flash_id_s id = adesto_read_signature();
    // TODO: printf the members of the 'adesto_flash_id_s' struct
    printf("\n manufacturer id - %x", id.manufacturer_id);
    printf("\n device_id1 - %x", id.device_id_1);
    printf("\n device_id2 - %x", id.device_id_2);
    printf("\n extended_device_id - %x", id.extended_device_id);

    vTaskDelay(500);
  }
}

void main(void) {
  xTaskCreate(spi_task, "SPI", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
}